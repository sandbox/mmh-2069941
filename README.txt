
-- SUMMARY --
This simple module allows you to flush the Varnish cache from the Drupal admin.
You can clear a specific hostname or all hosts.
It is possible to clear either a specific URL or the entire cache.
Regular expressions are supported.

This module requires the Varnish module,
found at https://drupal.org/project/varnish

